const csrf = require('csurf');
const properties = require('../properties/properties')

let exceptionHandler = (req, res, next) => {
    if (properties.ENV == 'Production') {
        const csrfProtection = csrf({ cookie: { httpOnly: true, sameSite: 'strict', secure: true }, ignoreMethods: ['POST', 'GET'] });
        csrfProtection(req, res, next);
        /* const csrfProtection = csrf({ cookie: { httpOnly: true, sameSite: 'strict', secure: true  } });
        csrfProtection(req, res, next) */
    } else {
        const csrfProtection = csrf({ cookie: true, ignoreMethods: ['POST', 'GET'] });
        /* const csrfProtection = csrf({ cookie: { httpOnly: true, sameSite: 'strict', secure: true  }, ignoreMethods: ['POST', 'GET'] }); */
        csrfProtection(req, res, next);
    }
}

module.exports = { exceptionHandler }