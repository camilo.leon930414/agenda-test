import { Component, OnInit, ViewChild } from '@angular/core';
import { ServicesService } from '../../../services/services.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
    public dataSource: any = new MatTableDataSource();
    public displayedColumns: string[] = ['name', 'tel', 'birthdate', 'address', 'email', 'year', 'edit', 'delete'];
    @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

    constructor(private service: ServicesService) {
    }
    ngOnInit(): void {
        this.getData();
    }

    getData() {
        const params: any = [];
        const SpName = 'spGetUsers';
        this.service.dinamicSpPost(params, SpName).subscribe((response: any) => {
            console.log(response)
            if (response.code == 200) {
                try {
                    let dataDescrypt = this.service.get(response.data);
                    let data = dataDescrypt != null ? JSON.parse(dataDescrypt) : null;
                    if (data != null) {
                        let res = data;
                        this.dataSource.data = res;
                        this.dataSource.paginator = this.paginator;
                    } else {
                        console.log('faile sp data')
                    }
                } catch (error) {
                    console.log(error)
                }
            } else {
                console.log('faile sp call')
            }
        })
    }
    delete(id: any) {
        const params: any = [{
            spName: 'id',
            spParam: id,
            type: 'Int'
        },];
        const SpName = 'spDeleteUsers';
        this.service.dinamicSpPost(params, SpName).subscribe((response: any) => {
            if (response.code == 200) {
                try {
                    this.getData();
                } catch (error) {
                    console.log(error)
                }
            } else {
                console.log('faile sp call')
            }
        })
    }
    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    getYear(date: any) {
        let hoy = new Date()
        let fechaNacimiento = new Date(date)
        let edad = hoy.getFullYear() - fechaNacimiento.getFullYear()
        let diferenciaMeses = hoy.getMonth() - fechaNacimiento.getMonth()
        if (
            diferenciaMeses < 0 ||
            (diferenciaMeses === 0 && hoy.getDate() < fechaNacimiento.getDate())
        ) {
            edad--
        }
        return edad
    }
}
