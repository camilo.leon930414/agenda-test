import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../../services/services.service';
import * as moment from 'moment';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  public data: any = {
    id: null,
    first_name: null,
    last_name: null,
    tel: null,
    birthdate: null,
    address: null,
    email: null
  };

  constructor(private service: ServicesService, private router: Router, private rutaActiva: ActivatedRoute) {
  }
  ngOnInit(): void {
    let id = this.rutaActiva.snapshot.params['id'];
    this.getdata(id);
  }

  getdata(id: any) {
    const params: any = [{
      spName: 'id',
      spParam: id,
      type: 'Int'
    },];
    const SpName = 'spGetUserId';
    this.service.dinamicSpPost(params, SpName).subscribe((response: any) => {
      if (response.code == 200) {
        try {
          let dataDescrypt = this.service.get(response.data);
          let dataPerson = dataDescrypt != null ? JSON.parse(dataDescrypt) : null;
          if (dataPerson.length > 0) {
            this.data = dataPerson[0];
            this.data.birthdate = new Date(this.data.birthdate).toISOString();
          }
        } catch (error) {
          console.log(error)
        }
      } else {
        console.log('faile sp call')
      }
    })
  }

  UpdateUser() {
    const params: any = [
      {
        spName: 'id',
        spParam: this.data.id,
        type: 'Int'
      }, {
        spName: 'first_name',
        spParam: this.data.first_name,
        type: 'VarChar'
      },
      {
        spName: 'last_name',
        spParam: this.data.last_name,
        type: 'VarChar'
      },
      {
        spName: 'tel',
        spParam: this.data.tel,
        type: 'VarChar'
      },
      {
        spName: 'birthdate',
        spParam: this.dateFormat(this.data.birthdate, 'yyyy/MM/dd'),
        type: 'VarChar'
      },
      {
        spName: 'address',
        spParam: this.data.address,
        type: 'VarChar'
      },
      {
        spName: 'email',
        spParam: this.data.email,
        type: 'VarChar'
      },];
    const SpName = 'spUpdateUser';
    let flag = false;
    Object.keys(this.data).forEach(index => {
      if (index != 'deleted_at') {
        if (this.data[index] == null || this.data[index] == '') {
          flag = true;
        }
      }
    });
    if (flag) {
      console.log('datos nulls')
      return
    }
    this.service.dinamicSpPost(params, SpName).subscribe((response: any) => {
      if (response.code == 200) {
        try {
          console.log('success')
          this.router.navigate(['/petition/list']);
        } catch (error) {
          console.log(error)
        }
      } else {
        console.log('faile sp call')
      }
    })
  }

  dateFormat(input_D: any, format_D: any) {
    // input date parsed
    const date = new Date(input_D);

    //extracting parts of date string
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();

    //to replace month
    format_D = format_D.replace("MM", month.toString().padStart(2, "0"));

    //to replace year
    if (format_D.indexOf("yyyy") > -1) {
      format_D = format_D.replace("yyyy", year.toString());
    } else if (format_D.indexOf("yy") > -1) {
      format_D = format_D.replace("yy", year.toString().substr(2, 2));
    }

    //to replace day
    format_D = format_D.replace("dd", day.toString().padStart(2, "0"));

    return format_D;
  }
}
