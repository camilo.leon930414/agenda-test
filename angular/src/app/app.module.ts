import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteRoutingModule } from './route/route-routing.module';
/* material */
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatGridListModule} from '@angular/material/grid-list';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
/* http */
import {
    HttpClientModule,
    HTTP_INTERCEPTORS,
    HttpClientXsrfModule,
} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
/* interceptores */
import { LoaderService } from './services/loader.service';
import { LoaderInterceptorService } from './services/loader-interceptor.service';
/* icon */
import { MatTooltipModule } from '@angular/material/tooltip';
/* interceptor */
import { CreateComponent } from './components/petitions/create/create.component';
/* views */
import { ListComponent } from './components/petitions/list/list.component';
import { LoaderComponent } from './components/loader/loader.component';
import { UpdateComponent } from './components/petitions/update/update.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateComponent,
    ListComponent,
    LoaderComponent,
    UpdateComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouteRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    MatDividerModule,
    MatGridListModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatTooltipModule,
    HttpClientXsrfModule,
    MatSlideToggleModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [
    LoaderService,
    {
        provide: HTTP_INTERCEPTORS,
        useClass: LoaderInterceptorService,
        multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
