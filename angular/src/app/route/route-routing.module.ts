import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
/* componentes */
import { CreateComponent } from '../components/petitions/create/create.component';
import { ListComponent } from '../components/petitions/list/list.component';
import { UpdateComponent } from '../components/petitions/update/update.component';

const routes: Routes = [
  {                                          // removed square bracket
    path: '',
    redirectTo: 'petition/list',
    pathMatch: 'full'
  },
  { path: 'petition/create', component: CreateComponent },
  { path: 'petition/list', component: ListComponent },
  { path: 'petition/update/:id', component: UpdateComponent },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RouteRoutingModule { }
